﻿using AerodromWPF.Model;
using AerodromWPF.Registracija;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnAerodromi_Click(object sender, RoutedEventArgs e)
        {
            AerodromiWindow aerodromiWindow = new AerodromiWindow();
            aerodromiWindow.Show();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            KorisniciWindow korisniciWindow = new KorisniciWindow();
            korisniciWindow.Show();
        }

        private void BtnLetovi_Click(object sender, RoutedEventArgs e)
        {
            LetWindow letWindow = new LetWindow();
            letWindow.Show();
        }

        private void BtnAvioni_Click(object sender, RoutedEventArgs e)
        {
            AvionWindow avionWindow = new AvionWindow();
            avionWindow.Show();
        }

        private void BtnAKpompanija_Click(object sender, RoutedEventArgs e)
        {
            AKompanijaWindow kompanijaWindow = new AKompanijaWindow();
            kompanijaWindow.Show();
        }

        private void BtnKarte_Click(object sender, RoutedEventArgs e)
        {
            KartaWindow kartaWindow = new KartaWindow();
            kartaWindow.Show();
        }

        private void BtnPretraga_Click(object sender, RoutedEventArgs e)
        {
            PretragaEntitetaWindow pew = new PretragaEntitetaWindow();
            pew.Show();
        }

        private void BtnSortiranje_Click(object sender, RoutedEventArgs e)
        {
            SortiranjeWindow sw = new SortiranjeWindow();
            sw.Show();
        }

        private void BtnOdjava_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow lw = new LoginWindow();
            lw.Show();
            this.Close();
        }
    }
}
