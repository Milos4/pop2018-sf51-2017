﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaAviona
{
    /// <summary>
    /// Interaction logic for PretragaPoAKompanijama.xaml
    /// </summary>
    public partial class PretragaPoAKompanijama : Window
    {
        public ObservableCollection<Avion> AvionPoAvioKompanijama { get; set; }
        public PretragaPoAKompanijama()
        {
            InitializeComponent();
            CBAvio.ItemsSource = Data.Instance.AKompanije;
            CBAvio.DisplayMemberPath = "Naziv";
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGAvio.ItemsSource = null;
            AvionPoAvioKompanijama = new ObservableCollection<Avion>();
            AKompanija avio = (AKompanija)CBAvio.SelectedItem;
            string ak = avio.Naziv.ToString();

            foreach(Avion avion in Data.Instance.Avioni)
            {
                if(avion.AKompanija == ak)
                {
                    AvionPoAvioKompanijama.Add(avion);
                }

            }
            DGAvio.ItemsSource = AvionPoAvioKompanijama;
        }
    }
}
