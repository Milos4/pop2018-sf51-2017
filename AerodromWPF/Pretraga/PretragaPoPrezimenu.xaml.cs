﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.Pretraga
{
    /// <summary>
    /// Interaction logic for PretragaPoPrezimenu.xaml
    /// </summary>
    public partial class PretragaPoPrezimenu : Window
    {
        public ObservableCollection<Korisnik> KorisniciPoPrezimenu { get; set; }
        public PretragaPoPrezimenu()
        {

            InitializeComponent();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGPrezime.ItemsSource = null;
            KorisniciPoPrezimenu = new ObservableCollection<Korisnik>();
            string prezime = TxtPrezime.Text;
            foreach (Korisnik korisnik in Data.Instance.Korisnici)
            {
                if (korisnik.Prezime == prezime)
                {
                    KorisniciPoPrezimenu.Add(korisnik);

                }

            }
            DGPrezime.ItemsSource = KorisniciPoPrezimenu;
        }
    }
}
