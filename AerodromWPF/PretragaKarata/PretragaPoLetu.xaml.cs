﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaKarata
{
    /// <summary>
    /// Interaction logic for PretragaPoLetu.xaml
    /// </summary>
    public partial class PretragaPoLetu : Window
    {
        public ObservableCollection<Karta> KartaPoLetu { get; set; }
        public PretragaPoLetu()
        {
            InitializeComponent();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGBroju.ItemsSource = null;
            KartaPoLetu = new ObservableCollection<Karta>();
            string sifra = TxtBroj.Text;
            foreach (Karta karta in Data.Instance.Karte)
            {
                if (karta.BrLeta == sifra)
                {
                    KartaPoLetu.Add(karta);

                }

            }
            DGBroju.ItemsSource = KartaPoLetu;
        }
    }
}
