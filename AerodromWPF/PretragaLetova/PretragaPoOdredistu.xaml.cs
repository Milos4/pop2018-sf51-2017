﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaLetova
{
    /// <summary>
    /// Interaction logic for PretragaPoOdredistu.xaml
    /// </summary>
    public partial class PretragaPoOdredistu : Window
    {
        public ObservableCollection<Let> LetaPoOdredistu { get; set; }
        public PretragaPoOdredistu()
        {
            InitializeComponent();
            CBOdrediste.ItemsSource = Data.Instance.Aerodromi;
            CBOdrediste.DisplayMemberPath = "Grad";
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGOdrediste.ItemsSource = null;
            LetaPoOdredistu = new ObservableCollection<Let>();
            Aerodrom grad = (Aerodrom)CBOdrediste.SelectedItem;
            string grad1 = grad.Grad.ToString();
            foreach (Let let in Data.Instance.Letovi)
            {
                if (let.Odrediste == grad1)
                {
                    LetaPoOdredistu.Add(let);

                }

            }
            DGOdrediste.ItemsSource = LetaPoOdredistu;
        }
    }
}
