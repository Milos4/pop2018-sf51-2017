﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AerodromiWindow.xaml
    /// </summary>
    public partial class AerodromiWindow : Window
    {
        List<Aerodrom> akticniAerodromi;
        public AerodromiWindow()
        {
            akticniAerodromi = new List<Aerodrom>();
            InitializeComponent();
            foreach (Aerodrom aerodrom in Data.Instance.Aerodromi)
            {

                if (aerodrom.Aktivan == true)
                {
                    akticniAerodromi.Add(aerodrom);
                }

            }
            DGAerodromi.ItemsSource = akticniAerodromi;
        }

 

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AddNEditWindow aew = new AddNEditWindow(new Aerodrom(), AddNEditWindow.EOpcija.DODAVANJE);
            aew.ShowDialog();
           
        }

        private void BtnIzmjeni_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom aerodrom = (Aerodrom)DGAerodromi.SelectedItem;


            if (SelektovanAerodrom(aerodrom))
            {
                Aerodrom stari = aerodrom.Clone() as Aerodrom;
                AddNEditWindow edit = new AddNEditWindow(aerodrom, AddNEditWindow.EOpcija.IZMJENA);
                if ((edit.ShowDialog()) == true)
                {
                    int index = IndexAerodroma(stari);
                }
            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Aerodrom aerodrom = (Aerodrom)DGAerodromi.SelectedItem;

            if (SelektovanAerodrom(aerodrom))
            {
                if (MessageBox.Show("Da li ste sigurni", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {

                    SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                    {
                        aerodrom.Aktivan = false;
                        string insert = "update Aerodrom set Aktivan='"
                             + aerodrom.Aktivan +
                             "' where Sifra='" + aerodrom.Sifra + "' ; ";
                        SqlCommand cmd = new SqlCommand(insert, con);

                        con.Open();

                        cmd.ExecuteNonQuery();
                        con.Close();
                    }

                }
            }
        }

        public int IndexAerodroma(Aerodrom aerodrom)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.Aerodromi.Count; i++)
            {
                if (Data.Instance.Aerodromi[i].Sifra.Equals(aerodrom.Sifra))
                {
                    index = i;
                
                }
            }
            return index;
        }

        private bool SelektovanAerodrom(Aerodrom aerodrom)
        {
            if (aerodrom == null)
            {
                MessageBox.Show("Nije selektovan aerodrom! ");
                return false;
            }
            return true;
        }

       
    }
}
