﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AddNEditWindowAvion.xaml
    /// </summary>
    public partial class AddNEditWindowAvion : Window
    {
        List<AKompanija> aKompanije;
        public enum EOpcija { DODAVANJE, IZMJENA };
        private EOpcija opcija;
        private Avion avion;
        public AddNEditWindowAvion(Avion avion, EOpcija opcija)
        {
            aKompanije = new List<AKompanija>(); 
            this.avion = avion;
            this.opcija = opcija;
            this.DataContext = avion;
            InitializeComponent();

            BrLetaCB.ItemsSource = Data.Instance.Letovi;
            BrLetaCB.DisplayMemberPath = "Sifra";
            foreach (AKompanija ak in Data.Instance.AKompanije)
            {
                if (ak.Aktivan == true)
                {
                    aKompanije.Add(ak);
                }
            }

                AKompanijaCB.ItemsSource = aKompanije;
                AKompanijaCB.DisplayMemberPath = "Naziv";
            
            if (opcija.Equals(EOpcija.IZMJENA))
            {
                BrLetaCB.IsEnabled = false;
            }
        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            Let brLeta = (Let)BrLetaCB.SelectedItem;
            AKompanija aKompanija = (AKompanija)AKompanijaCB.SelectedItem;
            avion.AKompanija = aKompanija.Naziv.ToString();

            if (opcija.Equals(EOpcija.IZMJENA))
            {
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                {
                    string insert = "update Avion set BrLeta='"
                         + avion.BrLeta + "',SBiznis='" + avion.SBiznis + "',SEkonomske='" + avion.SEkonomske + "',AKompanija='" + avion.AKompanija + "',Aktivan='" + avion.Aktivan +
                         "' where BrLeta='" + avion.BrLeta + "' ; ";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    con.Open();

                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            if (opcija.Equals(EOpcija.DODAVANJE) && !Postoji(avion.BrLeta))
            {
                avion.BrLeta = brLeta.Sifra.ToString();
                avion.AKompanija = aKompanija.Naziv.ToString();
                avion.Aktivan = true;
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                {
                    string insert = "insert into Avion(BrLeta,SBiznis,SEkonomske,AKompanija,Aktivan) values('"
                         + avion.BrLeta + "','" + avion.SBiznis + "','" + avion.SEkonomske + "','"
                         + avion.AKompanija + "','" + avion.Aktivan 
                         + "') ";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    con.Open();

                    cmd.ExecuteNonQuery();
                    con.Close();

                    Data.Instance.Avioni.Add(avion);

                }
            }
        }
        private void Odustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }
        public bool Postoji(string brLeta)
        {
            foreach (Avion avion in Data.Instance.Avioni)
            {
                if (avion.BrLeta.Equals(brLeta))
                {
                    return true;
                }
            }
            return false;
        }

  
    }
}
