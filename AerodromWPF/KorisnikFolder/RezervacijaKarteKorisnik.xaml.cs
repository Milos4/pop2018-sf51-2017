﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.KorisnikFolder
{
    
    /// <summary>
    /// Interaction logic for RezervacijaKarteKorisnik.xaml
    /// </summary>
    public partial class RezervacijaKarteKorisnik : Window
    {
        private Korisnik korisnik;
        private string polazak1;
        private string dolazak1;

        public ObservableCollection<Let> ListaLetova { get; set; }
        public RezervacijaKarteKorisnik(Korisnik korisnik)
        {
            this.korisnik = korisnik;
            ListaLetova = new ObservableCollection<Let>();
            InitializeComponent();
            PolazakCB.ItemsSource = Data.Instance.Aerodromi;
            PolazakCB.DisplayMemberPath = "Grad";

            DolazakCB.ItemsSource = Data.Instance.Aerodromi;
            DolazakCB.DisplayMemberPath = "Grad";
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            Aerodrom polazak = (Aerodrom)PolazakCB.SelectedItem;
            Aerodrom dolazak = (Aerodrom)DolazakCB.SelectedItem;
            polazak1 = polazak.Grad;
            dolazak1 = dolazak.Grad;
            string VrijemePolaska = PolazakDP.ToString();

            foreach (Let let in Data.Instance.Letovi)
            {
                if (let.Destinacija.Equals(polazak1) && let.Odrediste.Equals(dolazak1) && let.VrijemePolaska.ToString().Equals(VrijemePolaska))
                {
                    ListaLetova.Add(let);

                }


            }
            if (ListaLetova != null)
            {
                IspisLetova edit = new IspisLetova(korisnik, ListaLetova);
                edit.Show();
                this.Close();
            }
        }


    }

}

