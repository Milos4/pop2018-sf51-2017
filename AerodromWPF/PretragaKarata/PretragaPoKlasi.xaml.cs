﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaKarata
{
    /// <summary>
    /// Interaction logic for PretragaPoKlasi.xaml
    /// </summary>
    public partial class PretragaPoKlasi : Window
    {
        public ObservableCollection<Karta> KartaPoKlasi { get; set; }
        public PretragaPoKlasi()
        {
            InitializeComponent();
            CBKlasa.ItemsSource = Enum.GetValues(typeof(Klasa));
            CBKlasa.SelectedItem = Klasa.Ekonomska;
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGKarta.ItemsSource = null;
            KartaPoKlasi = new ObservableCollection<Karta>();
            Klasa klasa = (Klasa)CBKlasa.SelectedItem;
            string klasa1 = klasa.ToString();
            foreach (Karta karta in Data.Instance.Karte)
            {
                if (karta.Klasa == klasa1 && karta.Aktivan == true )
                {
                    KartaPoKlasi.Add(karta);

                }

            }
            DGKarta.ItemsSource = KartaPoKlasi;
        }
    }
}
