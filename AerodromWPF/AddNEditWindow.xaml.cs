﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AddNEditWindow.xaml
    /// </summary>
    public partial class AddNEditWindow : Window
    {
        public enum EOpcija { DODAVANJE, IZMJENA }
        private EOpcija opcija;
        private Aerodrom aerodrom;
        private Aerodrom neizmjenjen;
        public AddNEditWindow(Aerodrom aerodrom, EOpcija opcija)
        {
            neizmjenjen = aerodrom;
            this.aerodrom = aerodrom;
            this.opcija = opcija;
            this.DataContext = aerodrom;

            InitializeComponent();

            if (opcija.Equals(EOpcija.IZMJENA))
            {

                TxtSifra.IsEnabled = false;

            }
        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (opcija.Equals(EOpcija.IZMJENA))
            {
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                {
                    string insert = "update Aerodrom set Sifra='"
                         + aerodrom.Sifra + "',Naziv='" + aerodrom.Naziv + "',Grad='" + aerodrom.Grad + "',Aktivan='" + aerodrom.Aktivan + 
                         "' where Sifra='" + aerodrom.Sifra + "' ; ";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    con.Open();

                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            if (opcija.Equals(EOpcija.DODAVANJE) && !PostojiAerodrom(aerodrom.Sifra))
            {
                aerodrom.Aktivan = true;

                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                {
                    string insert = "insert into Aerodrom(Sifra,Naziv,Grad,Aktivan) values('"
                         + aerodrom.Sifra + "','" + aerodrom.Naziv + "','" + aerodrom.Grad + "','"
                         + aerodrom.Aktivan
                         + "') ";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    con.Open();

                    cmd.ExecuteNonQuery();
                    con.Close();
                    Data.Instance.Aerodromi.Add(aerodrom);
                }
            }

        }

        private void Odustani_Click(object sender, RoutedEventArgs e)
        {

            {
                this.DialogResult = false;
            }

            
            

        }
    
        private bool PostojiAerodrom(string sifra)
        {

            foreach (Aerodrom aerodrom in Data.Instance.Aerodromi)
            {
                if (aerodrom.Sifra.Equals(sifra))
                {
                    return true;

                }

            }
            return false;
        }
        public int IndexAerodroma(Aerodrom aerodrom)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.Aerodromi.Count; i++)
            {
                if (Data.Instance.Aerodromi[i].Sifra.Equals(aerodrom.Sifra))
                {
                    index = i;

                }
            }
            return index;
        }
        public void SacuvajSveAerodrome()
        {
            XmlWriter writer = XmlWriter.Create("..//..//Data//Aerodromi.xml");

            writer.WriteStartElement("aerodromi");
            foreach (Aerodrom aerodrom in Data.Instance.Aerodromi)
            {
                writer.WriteStartElement("aerodrom");
                writer.WriteAttributeString("sifra", aerodrom.Sifra);
                writer.WriteAttributeString("naziv", aerodrom.Naziv);
                writer.WriteAttributeString("grad", aerodrom.Grad);
                writer.WriteEndElement();
            }

            writer.WriteEndDocument();
            writer.Close();


        }
    }   

}
