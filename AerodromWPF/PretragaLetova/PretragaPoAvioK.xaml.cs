﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaLetova
{
    /// <summary>
    /// Interaction logic for PretragaPoAvioK.xaml
    /// </summary>
    public partial class PretragaPoAvioK : Window
    {
        public ObservableCollection<Let> LetaPoAvio { get; set; }
        public PretragaPoAvioK()
        {
            InitializeComponent();
            CBAvio.ItemsSource = Data.Instance.AKompanije;
            CBAvio.DisplayMemberPath = "Naziv";
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            CBAvio.ItemsSource = null;
            LetaPoAvio = new ObservableCollection<Let>();
            AKompanija aK = (AKompanija)CBAvio.SelectedItem;
            string ak1 = aK.ToString();

        }
    }
}
