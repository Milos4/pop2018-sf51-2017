﻿using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.KorisnikFolder
{
    /// <summary>
    /// Interaction logic for PregledProfilaWindow.xaml
    /// </summary>
    public partial class PregledProfilaWindow : Window
    {
        Korisnik stari;
        Korisnik korisnik;
        public PregledProfilaWindow(Korisnik korisnik , Korisnik stari)
        {

            this.korisnik = korisnik;
            this.DataContext = stari;
            InitializeComponent();
            KIme.IsEnabled = false;

        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)

        {
            korisnik.Prezime = Prezime.Text.ToString();
            korisnik.Ime = Ime.Text.ToString();
            korisnik.Lozinka = Lozinka.Text.ToString();
            korisnik.Email = Email.Text.ToString();
            korisnik.Adresa = Adresa.Text.ToString();
            stari = korisnik;

            this.DialogResult = true;
        }

        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DataContext = stari;
            this.Close();
        }
    }
}
