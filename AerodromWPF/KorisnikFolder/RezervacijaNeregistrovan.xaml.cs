﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.KorisnikFolder
{
    /// <summary>
    /// Interaction logic for RezervacijaNeregistrovan.xaml
    /// </summary>
    public partial class RezervacijaNeregistrovan : Window
    {
        public ObservableCollection<Let> ListaLetova { get; set; }
        public enum Klasa { Ekonomska, Biznis };
        public enum Vrsta { Jednosmjerna, Povratna };
        private Let let;
        private Karta karta = new Karta();
        private Sjedista sjedista = new Sjedista { BrSlobodnih = new List<int>(), BrZausetih = new List<int>() };
        public RezervacijaNeregistrovan(Let let)
        {
            ListaLetova = new ObservableCollection<Let>();
            InitializeComponent();
            this.let = let;
            VrstaCB.ItemsSource = Enum.GetValues(typeof(Vrsta));
            KlasaCB.ItemsSource = Enum.GetValues(typeof(Klasa));
            VrstaCB.SelectedItem = Vrsta.Jednosmjerna;
            KlasaCB.SelectedItem = Klasa.Ekonomska;
            LblSifra.Content = let.Sifra;
            LblIz.Content = let.Destinacija;
            LblZa.Content = let.Odrediste;
            LblU.Content = let.VrijemePolaska;
        }

        private void BtnDalje_Click(object sender, RoutedEventArgs e)
        {
            int b = 0;
            int ek = 0;
            int izabrno = Convert.ToInt32(TBbrsjedista.Text);
            Vrsta vrsta = (Vrsta)VrstaCB.SelectedItem;
            Klasa klasa = (Klasa)KlasaCB.SelectedItem;
            string vrstaKarte = vrsta.ToString();
            foreach (Avion avion in Data.Instance.Avioni)
            {
                if (avion.BrLeta == let.Sifra)
                {
                    b = avion.SBiznis;
                    ek = avion.SEkonomske;
                }
            }


            if (vrsta == Vrsta.Jednosmjerna)
            {
                if (klasa == Klasa.Biznis)
                {
                    sjedista.PopuniBiznis(b);
                    foreach (Karta karta in Data.Instance.Karte)
                    {
                        if (karta.BrLeta == let.Sifra && karta.Klasa == "Biznis")
                        {
                            int sjedisteB = Int32.Parse(karta.BrSjedista);
                            sjedista.BrZausetih.Add(sjedisteB);
                        }
                    }
                    if (sjedista.BrZausetih.Contains(izabrno) == false && sjedista.BrSlobodnih.Contains(izabrno) == true)
                    {
                        karta.BrLeta = let.Sifra;
                        karta.NKorisnika = ImeiPrezime.Text;
                        karta.Klasa = klasa.ToString();
                        double cijenaBiznis = let.Cijena;
                        karta.Cijena = cijenaBiznis + cijenaBiznis / 2;
                        karta.Kapija = "1A";
                        karta.BrSjedista = izabrno.ToString();
                        karta.Aktivan = true;
                        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                        {
                            string insert = "insert into Karta(BrLeta,BrSjedista,NKorisnika,Klasa,Kapija,Cijena,Aktivan) values('"
                                 + karta.BrLeta + "','" + karta.BrSjedista + "','" + karta.NKorisnika + "','" + karta.Klasa + "','" + karta.Kapija + "','" + karta.Cijena
                                + "','" + karta.Aktivan + "') ";
                            SqlCommand cmd = new SqlCommand(insert, con);

                            con.Open();

                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                        Data.Instance.Karte.Add(karta);
                        MessageBox.Show("Uspjesno ste rezervisali kartu! ");
                        this.Close();
                    }
                    else if (sjedista.BrZausetih.Contains(izabrno) == true && sjedista.BrSlobodnih.Contains(izabrno) == true)
                    {
                        MessageBox.Show("Mjesto je vec rezervisano ");

                    }
                    else
                    {
                        MessageBox.Show("Lose unijet format! ");
                    }
                }
                if (klasa == Klasa.Ekonomska)
                {
                    sjedista.PopuniEkonomska(ek);
                    foreach (Karta karta in Data.Instance.Karte)
                    {

                        if (karta.BrLeta == let.Sifra && karta.Klasa == "Ekonomska")
                        {
                            int sjedisteE = Int32.Parse(karta.BrSjedista);
                            sjedista.BrZausetih.Add(sjedisteE);
                        }
                    }
                    if (sjedista.BrZausetih.Contains(izabrno) == false && sjedista.BrSlobodnih.Contains(izabrno) == true)
                    {
                        karta.BrLeta = let.Sifra;
                        karta.NKorisnika = ImeiPrezime.Text;
                        karta.Klasa = klasa.ToString();
                        double cijenaEkonomske = let.Cijena;
                        karta.Cijena = cijenaEkonomske;
                        karta.Kapija = "1A";
                        karta.BrSjedista = izabrno.ToString();
                        karta.Aktivan = true;
                        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                        {
                            string insert = "insert into Karta(BrLeta,BrSjedista,NKorisnika,Klasa,Kapija,Cijena,Aktivan) values('"
                                 + karta.BrLeta + "','" + karta.BrSjedista + "','" + karta.NKorisnika + "','" + karta.Klasa + "','" + karta.Kapija + "','" + karta.Cijena
                                + "','" + karta.Aktivan + "') ";
                            SqlCommand cmd = new SqlCommand(insert, con);

                            con.Open();

                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                        Data.Instance.Karte.Add(karta);
                        MessageBox.Show("Uspjesno ste rezervisali kartu! ");
                        this.Close();
                    }
                    else if (sjedista.BrZausetih.Contains(izabrno) == true && sjedista.BrSlobodnih.Contains(izabrno) == true)
                    {
                        MessageBox.Show("Mjesto je vec rezervisano ");

                    }
                    else
                    {
                        MessageBox.Show("Lose unijet format! ");
                    }
                }
            }
            else if (vrsta == Vrsta.Povratna)
            {
                if (klasa == Klasa.Biznis)
                {
                    sjedista.PopuniBiznis(b);
                    foreach (Karta karta in Data.Instance.Karte)
                    {
                        if (karta.BrLeta == let.Sifra && karta.Klasa == "Biznis")
                        {
                            int sjedisteB = Int32.Parse(karta.BrSjedista);
                            sjedista.BrZausetih.Add(sjedisteB);
                        }
                    }
                    if (sjedista.BrZausetih.Contains(izabrno) == false && sjedista.BrSlobodnih.Contains(izabrno) == true)
                    {
                        karta.BrLeta = let.Sifra;
                        karta.NKorisnika = ImeiPrezime.Text;
                        karta.Klasa = klasa.ToString();
                        double cijenaBiznis = let.Cijena;
                        karta.Cijena = cijenaBiznis + cijenaBiznis / 2;
                        karta.Kapija = "1A";
                        karta.BrSjedista = izabrno.ToString();
                        karta.Aktivan = true;
                        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                        {
                            string insert = "insert into Karta(BrLeta,BrSjedista,NKorisnika,Klasa,Kapija,Cijena,Aktivan) values('"
                                 + karta.BrLeta + "','" + karta.BrSjedista + "','" + karta.NKorisnika + "','" + karta.Klasa + "','" + karta.Kapija + "','" + karta.Cijena
                                + "','" + karta.Aktivan + "') ";
                            SqlCommand cmd = new SqlCommand(insert, con);

                            con.Open();

                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                        Data.Instance.Karte.Add(karta);

                        string polazak = let.Odrediste; ;
                        string dolazak = let.Destinacija;

                        foreach (Let let in Data.Instance.Letovi)
                        {
                            if (let.Destinacija.Equals(polazak) && let.Odrediste.Equals(dolazak))
                            {
                                ListaLetova.Add(let);

                            }


                        }
                        if (ListaLetova != null)
                        {
                            IspisLetovaNeregistrovan edit = new IspisLetovaNeregistrovan( ListaLetova);
                            edit.Show();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Nema povratnog leta");
                        }
                        MessageBox.Show("Uspjesno ste rezervisali kartu u ovom smjeru");
                        this.Close();
                    }
                    else if (sjedista.BrZausetih.Contains(izabrno) == true && sjedista.BrSlobodnih.Contains(izabrno) == true)
                    {
                        MessageBox.Show("Mjesto je vec rezervisano ");

                    }
                    else
                    {
                        MessageBox.Show("Lose unijet format! ");
                    }
                }
                if (klasa == Klasa.Ekonomska)
                {
                    sjedista.PopuniEkonomska(ek);
                    foreach (Karta karta in Data.Instance.Karte)
                    {

                        if (karta.BrLeta == let.Sifra && karta.Klasa == "Ekonomska")
                        {
                            int sjedisteE = Int32.Parse(karta.BrSjedista);
                            sjedista.BrZausetih.Add(sjedisteE);
                        }
                    }
                    if (sjedista.BrZausetih.Contains(izabrno) == false && sjedista.BrSlobodnih.Contains(izabrno) == true)
                    {
                        karta.BrLeta = let.Sifra;
                        karta.NKorisnika = ImeiPrezime.Text;
                        karta.Klasa = klasa.ToString();
                        double cijenaEkonomske = let.Cijena;
                        karta.Cijena = cijenaEkonomske;
                        karta.Kapija = "1A";
                        karta.BrSjedista = izabrno.ToString();
                        karta.Aktivan = true;
                        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                        {
                            string insert = "insert into Karta(BrLeta,BrSjedista,NKorisnika,Klasa,Kapija,Cijena,Aktivan) values('"
                                 + karta.BrLeta + "','" + karta.BrSjedista + "','" + karta.NKorisnika + "','" + karta.Klasa + "','" + karta.Kapija + "','" + karta.Cijena
                                + "','" + karta.Aktivan + "') ";
                            SqlCommand cmd = new SqlCommand(insert, con);

                            con.Open();

                            cmd.ExecuteNonQuery();
                            con.Close();
                        }

                        Data.Instance.Karte.Add(karta);
                        string polazak = let.Odrediste; ;
                        string dolazak = let.Destinacija;

                        foreach (Let let in Data.Instance.Letovi)
                        {
                            if (let.Destinacija.Equals(polazak) && let.Odrediste.Equals(dolazak))
                            {
                                ListaLetova.Add(let);

                            }


                        }
                        if (ListaLetova != null)
                        {
                            IspisLetovaNeregistrovan edit = new IspisLetovaNeregistrovan(ListaLetova);
                            edit.Show();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Nema povratnog leta");
                        }
                        MessageBox.Show("Uspjesno ste rezervisali kartu u ovom smjeru");
                        this.Close();

                    }
                    else if (sjedista.BrZausetih.Contains(izabrno) == true && sjedista.BrSlobodnih.Contains(izabrno) == true)
                    {
                        MessageBox.Show("Mjesto je vec rezervisano ");

                    }
                    else
                    {
                        MessageBox.Show("Lose unijet format! ");
                    }
                }

            }
        }
    }
}
