﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaKarata
{
    /// <summary>
    /// Interaction logic for PretragaPoKorisniku.xaml
    /// </summary>
    public partial class PretragaPoKorisniku : Window
    {
        public ObservableCollection<Karta> KartaPoNazivu { get; set; }
        public PretragaPoKorisniku()
        {
            InitializeComponent();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGKorisnik.ItemsSource = null;
            KartaPoNazivu = new ObservableCollection<Karta>();
            string naziv = TxtKorisnik.Text;
            foreach (Karta karta in Data.Instance.Karte)
            {
                if (karta.NKorisnika == naziv)
                {
                    KartaPoNazivu.Add(karta);

                }

            }
            DGKorisnik.ItemsSource = KartaPoNazivu;
        }
    }
}
