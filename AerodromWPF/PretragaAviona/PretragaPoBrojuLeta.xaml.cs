﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaAviona
{
    /// <summary>
    /// Interaction logic for PretragaPoBrojuLeta.xaml
    /// </summary>
    public partial class PretragaPoBrojuLeta : Window
    {
        public ObservableCollection<Avion> AvionPoBroju { get; set; }
        public PretragaPoBrojuLeta()
        {
            InitializeComponent();
            CBLet.ItemsSource = Data.Instance.Letovi;
            CBLet.DisplayMemberPath = "Sifra";
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGLet.ItemsSource = null;
            AvionPoBroju = new ObservableCollection<Avion>();
            Let let = (Let)CBLet.SelectedItem;
            string sifra = let.Sifra.ToString();

            foreach (Avion avion in Data.Instance.Avioni)
            {
                if (avion.BrLeta == sifra)
                {
                    AvionPoBroju.Add(avion);
                }

            }
            DGLet.ItemsSource = AvionPoBroju;
        }
    }
}
