﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AKompanijaWindow.xaml
    /// </summary>
    public partial class AKompanijaWindow : Window
    {
        List<AKompanija> aktivneKompanije;
        public AKompanijaWindow()
        {
            aktivneKompanije = new List<AKompanija>();
            InitializeComponent();
            foreach (AKompanija aKompanija in Data.Instance.AKompanije)
            {

                if (aKompanija.Aktivan == true)
                {
                    aktivneKompanije.Add(aKompanija);
                }

            }
            DGKompanije.ItemsSource = aktivneKompanije;
        }
        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AddNEditWindowKompanija aewk = new AddNEditWindowKompanija(new AKompanija(), AddNEditWindowKompanija.EOpcija.DODAVANJE);
            aewk.ShowDialog();

        }

        private void BtnIzmjeni_Click(object sender, RoutedEventArgs e)
        {
            AKompanija aKompanija = (AKompanija)DGKompanije.SelectedItem;


            if (SelektovanKormpanija(aKompanija))
            {
                AKompanija stari = aKompanija.Clone() as AKompanija;
                AddNEditWindowKompanija edit = new AddNEditWindowKompanija(aKompanija, AddNEditWindowKompanija.EOpcija.IZMJENA);
                if ((edit.ShowDialog()) == true)
                {
                    int index = IndexAKompanije(stari);
                }
            }

        }

        private void BtnIzbrisi_Click(object sender, RoutedEventArgs e)
        {
            AKompanija aKompanija = (AKompanija)DGKompanije.SelectedItem;
            if (SelektovanKormpanija(aKompanija) && aKompanija.Aktivan)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    aKompanija.Aktivan = false;
                    SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                    {
                        string insert = "update AKompanija set Aktivan='"
                             + aKompanija.Aktivan +  "' where Sifra='" + aKompanija.Sifra + "' ; ";
                        SqlCommand cmd = new SqlCommand(insert, con);

                        con.Open();

                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }



        }



        public int IndexAKompanije(AKompanija aKompanija)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.AKompanije.Count; i++)
            {
                if (Data.Instance.AKompanije[i].Sifra.Equals(aKompanija.Sifra))
                {
                    index = i;

                }
            }
            return index;
        }

        private bool SelektovanKormpanija(AKompanija aKompanija)
        {
            if (aKompanija == null)
            {
                MessageBox.Show("Morate izavrati kompaniju! ");
                return false;
            }
            return true;
        }
    }
}
