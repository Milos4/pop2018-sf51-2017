﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AddNEditWindowKarte.xaml
    /// </summary>
    public partial class AddNEditWindowKarte : Window
    {
        public enum EOpcija { DODAVANJE, IZMJENA };
        private EOpcija opcija;
        private Karta karta;
        private string stariBrojSjedista;
        private string stariBrojLeta;
        public AddNEditWindowKarte(Karta karta, EOpcija opcija)
        {
            stariBrojSjedista = karta.BrSjedista;
            stariBrojLeta = karta.BrLeta;
            InitializeComponent();
            this.DataContext = karta;
            this.karta = karta;
            this.opcija = opcija;
            InitializeComponent();

            BrLeta.ItemsSource = Data.Instance.Letovi;
            BrLeta.DisplayMemberPath = "Sifra";


            if (opcija.Equals(EOpcija.IZMJENA))
            {
                KNaziv.IsEnabled = false;
            }


        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {


            this.DialogResult = true;
            Let brLeta = (Let)BrLeta.SelectedItem;
            this.karta.BrLeta = BrLeta.Text.ToString();
            if (opcija.Equals(EOpcija.IZMJENA))
            {

                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                {
                    string insert = "update Karta set BrLeta='"
                         + karta.BrLeta + "',BrSjedista='" + karta.BrSjedista + "',NKorisnika='" + karta.NKorisnika + "',Klasa='" + karta.Klasa + "',Kapija='" + karta.Kapija + "',Cijena='" + karta.Cijena
                        +  "',Aktivan='" + karta.Aktivan + "' where BrLeta ='" + stariBrojLeta + "' and BrSjedista ='" + stariBrojSjedista + "' ; ";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    con.Open();

                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            if (opcija.Equals(EOpcija.DODAVANJE) && !Postoji(karta.BrSjedista))
            {
                karta.NKorisnika = KNaziv.Text.ToString();
                karta.BrLeta = brLeta.Sifra.ToString();
                karta.Aktivan = true;
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                {
                    string insert = "insert into Karta(BrLeta,BrSjedista,NKorisnika,Klasa,Kapija,Cijena,Aktivan) values('"
                         + karta.BrLeta + "','" + karta.BrSjedista + "','" + karta.NKorisnika + "','" + karta.Klasa + "','" + karta.Kapija + "','" + karta.Cijena
                        + "','" + karta.Aktivan + "') ";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    con.Open();

                    cmd.ExecuteNonQuery();
                    con.Close();

                    Data.Instance.Karte.Add(karta);

                }
            }
        }
        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }
        public bool Postoji(string brSjedista)
        {
            foreach (Karta karta in Data.Instance.Karte)
            {
                if (karta.BrSjedista.Equals(brSjedista))
                {
                    return true;
                }
            }
            return false;
        }


    }
}

