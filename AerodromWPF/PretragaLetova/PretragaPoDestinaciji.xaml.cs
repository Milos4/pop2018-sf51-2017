﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaLetova
{
    /// <summary>
    /// Interaction logic for PretragaPoDestinaciji.xaml
    /// </summary>
    public partial class PretragaPoDestinaciji : Window
    {
        public ObservableCollection<Let> LetaPoDestinaciji { get; set; }
        public PretragaPoDestinaciji()
        {
            InitializeComponent();
            CBDestinacija.ItemsSource = Data.Instance.Aerodromi;
            CBDestinacija.DisplayMemberPath = "Grad";
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGDestinacija.ItemsSource = null;
            LetaPoDestinaciji = new ObservableCollection<Let>();
            Aerodrom grad = (Aerodrom)CBDestinacija.SelectedItem;
            string grad1 = grad.Grad.ToString();
            foreach (Let let in Data.Instance.Letovi)
            {
                if (let.Destinacija == grad1)
                {
                    LetaPoDestinaciji.Add(let);

                }

            }
            DGDestinacija.ItemsSource = LetaPoDestinaciji;
        }
    }
}
