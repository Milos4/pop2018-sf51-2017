﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Aerodrom : INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; Changed("Sifra"); }
        }

        private string naziv;

        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; Changed("Naziv"); }
        }

        private string grad;

        public string Grad
        {
            get { return grad; }
            set { grad = value; Changed("Grad"); }
        }

        private Boolean aktivan;

        public Boolean Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; Changed("Aktivan"); }
        }

        public void Changed(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }

        public object Clone()
        {
            var aerodrom = new Aerodrom
            {
                Sifra = this.sifra,
                Naziv = this.naziv,
                Grad = this.grad,
                Aktivan = this.aktivan

            };
            return aerodrom;
        }
/*
        public override string ToString()
        {
            return $"Sifra {Sifra}, Naziv {Naziv}, Grad {Grad}";
        }
        */
    }
}
