﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AddNEditWindowKorisnik.xaml
    /// </summary>
    public partial class AddNEditWindowKorisnik : Window
    {
       
        public enum EOpcija { DODAVANJE, IZMJENA }
        private EOpcija opcija;
        private Korisnik korisnik;


        public AddNEditWindowKorisnik(Korisnik korisnik, EOpcija opcija)
        {

            InitializeComponent();
            this.korisnik = korisnik;
            this.opcija = opcija;
            this.DataContext = korisnik;


            PolCB.ItemsSource = Enum.GetValues(typeof(Pol));
            TipCB.ItemsSource = Enum.GetValues(typeof(Tip));
            PolCB.SelectedItem = Pol.Muski;
            TipCB.SelectedItem = Tip.Admin;


            if (opcija.Equals(EOpcija.IZMJENA))
            {


                KIme.IsEnabled = false;
                PolCB.SelectedItem = this.korisnik.Pol;
                TipCB.SelectedItem = this.korisnik.Tip;


            }
        }

        private void BtnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            Pol pol = (Pol)PolCB.SelectedItem;
            Tip tip = (Tip)TipCB.SelectedItem;
            korisnik.Tip = tip.ToString();
            korisnik.Pol = pol.ToString();
            if (opcija.Equals(EOpcija.IZMJENA))
            {
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                {
                    string insert = "update Korisnik set KIme='"
                         + korisnik.KIme + "',Lozinka='" + korisnik.Lozinka + "',Ime='" + korisnik.Ime + "',Prezime='" + korisnik.Prezime + "',Email='" + korisnik.Email + "',Adresa='" + korisnik.Adresa
                        + "',Pol='" + korisnik.Pol + "',Tip='" + korisnik.Tip + "',Aktivan='" + korisnik.Aktivan + "' where KIme='" + korisnik.KIme + "' ; ";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    con.Open();

                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }



                if (opcija.Equals(EOpcija.DODAVANJE) && !PostojiKorisnik(korisnik.KIme))
            {
                korisnik.Aktivan = true;


                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                {
                    string insert = "insert into Korisnik(KIme,Lozinka,Ime,Prezime,Email,Adresa,Pol,Tip,Aktivan) values('"
                         + korisnik.KIme + "','" + korisnik.Lozinka + "','" + korisnik.Ime + "','" + korisnik.Prezime + "','" + korisnik.Email + "','" + korisnik.Adresa
                        + "','" + korisnik.Pol + "','" + korisnik.Tip + "','" + korisnik.Aktivan + "') ";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    con.Open();

                    cmd.ExecuteNonQuery();
                    con.Close();
                    Data.Instance.Korisnici.Add(korisnik);
                }

            }

        }
        private void BtnOdustani_Click(object sender, RoutedEventArgs e)
        {
            {
                this.DialogResult = false;
            }

        }

        private void PolCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TipCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private bool PostojiKorisnik(string sifra)
        {

            foreach (Korisnik korsnik in Data.Instance.Korisnici)
            {
                if (korsnik.KIme.Equals(KIme))
                {
                    return true;

                }

            }
            return false;
        }

    }
}
