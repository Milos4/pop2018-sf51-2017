﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AvionWindow.xaml
    /// </summary>
    public partial class AvionWindow : Window
    {
        List<Avion> aktivniAvioni;
        public AvionWindow()
        {
            aktivniAvioni = new List<Avion>();
            InitializeComponent();
            foreach (Avion avion in Data.Instance.Avioni)
            {
            
                if (avion.Aktivan == true)
                {
                    aktivniAvioni.Add(avion);
                }

            }
            DGAvioni.ItemsSource = aktivniAvioni;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AddNEditWindowAvion edit = new AddNEditWindowAvion(new Avion(), AddNEditWindowAvion.EOpcija.DODAVANJE);
            edit.ShowDialog();
        }

        private void BtnIzmjeni_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = (Avion)DGAvioni.SelectedItem;

            if (SelektovanAvion(avion))
            {
                Avion stari = avion.Clone() as Avion;
                AddNEditWindowAvion edit = new AddNEditWindowAvion(avion, AddNEditWindowAvion.EOpcija.IZMJENA);
                if ((edit.ShowDialog()) == true)
                {
                    int index = IndexAvion(stari);
                }
            }

        }

        private void BtnIzbrisi_Click(object sender, RoutedEventArgs e)
        {
            Avion avion = (Avion)DGAvioni.SelectedItem;

            if (SelektovanAvion(avion))
            {
                if (MessageBox.Show("Da li ste sigurni", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    avion.Aktivan = false;
                    SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                    {
                        string insert = "update Avion set Aktivan='"
                             + avion.Aktivan + 
                             "' where BrLeta='" + avion.BrLeta + "' ; ";
                        SqlCommand cmd = new SqlCommand(insert, con);

                        con.Open();

                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
        }
        private int IndexAvion(Avion avion)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.Avioni.Count; i++)
            {
                if (Data.Instance.Avioni[i].BrLeta.Equals(avion.BrLeta))
                {
                    index = i;

                }

            }
            return index;

        }
        private bool SelektovanAvion(Avion avion)
        {
            if (avion == null)
            {
                MessageBox.Show("Morate izabrati avion");
                return false;
            }
            return true;
        }
    }
}
