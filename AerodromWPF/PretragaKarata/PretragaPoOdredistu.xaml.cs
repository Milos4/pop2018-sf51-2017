﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaKarata
{
    /// <summary>
    /// Interaction logic for PretragaPoOdredistu.xaml
    /// </summary>
    public partial class PretragaPoOdredistu : Window
    {
        public ObservableCollection<Karta> KartePoOdredistu { get; set; }
        public ObservableCollection<Let> Letovi { get; set; }
        public PretragaPoOdredistu()
        {
            InitializeComponent();
            CBOdrediste.ItemsSource = Data.Instance.Letovi;
            CBOdrediste.DisplayMemberPath = "Odrediste";
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGKarte.ItemsSource = null;
            KartePoOdredistu = new ObservableCollection<Karta>();
            string odrediste = CBOdrediste.SelectedItem.ToString();
            foreach (Let let in Data.Instance.Letovi)
            {
                if (let.Odrediste == odrediste && let.Aktivan == true)
                {
                    Letovi.Add(let);

                }
            }
            DGKarte.ItemsSource = KartePoOdredistu;
        }
    }
}