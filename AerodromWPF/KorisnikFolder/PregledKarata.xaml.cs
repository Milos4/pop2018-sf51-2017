﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.KorisnikFolder
{
    /// <summary>
    /// Interaction logic for PregledKarata.xaml
    /// </summary>
    public partial class PregledKarata : Window
    {
        public ObservableCollection<Karta> KartaKorisnika { get; set; }
        public PregledKarata(Korisnik korisnik)
        {
            InitializeComponent();
            KartaKorisnika = new ObservableCollection<Karta>();
            foreach (Karta karta in Data.Instance.Karte)
            {
                
                if (karta.NKorisnika == korisnik.KIme)
                {
                    KartaKorisnika.Add(karta);
                }
            }
            DGKorisnik.ItemsSource = KartaKorisnika;
        }
    }
}
