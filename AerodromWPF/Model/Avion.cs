﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AerodromWPF.Model;

namespace AerodromWPF.Model
{
        public class Avion : INotifyPropertyChanged, ICloneable
        {
        Sjedista sjedista;
            public event PropertyChangedEventHandler PropertyChanged;

            

        
            private string brLeta;

            public string BrLeta
            {
                get { return brLeta; }
                set { brLeta = value; Changed("BrLeta"); }
            }

            private int sBiznis;

            public int SBiznis
            {
                get { return sBiznis; }
                set { sBiznis = value; Changed("SBiznis"); }
            }

            private int sEkonomske;

            public int SEkonomske
            {
                get { return sEkonomske; }
                set { sEkonomske = value; Changed("SEkonomske"); }
            }

            private string aKompanija;

            public string AKompanija
            {
                get { return aKompanija; }
                set { aKompanija = value; Changed("AKompanija"); }
            }

            private Boolean aktivan;


            public Boolean Aktivan
            {
                get { return aktivan; }
                set { aktivan = value; Changed("Aktivan"); }
            }

            public void Changed(string name)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(name));
                }

            }

        public void Popuni(int b, int e)
        {
            int x, y;
            x = b / 10;
            y = b - x * 10;
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    int c;
                    c = i * 10 + j;
                    sjedista.BrSlobodnih.Add(c);
                }
            }
            int q, w;
            q = e / 10;
            w = e - q * 10;
            for (int k = 0; k < q; k++)
            {
                for (int j = 0; j < w; j++)
                {
                    int c;
                    c = k * 10 + j;
                    sjedista.BrSlobodnih.Add(c);
                }
            }
        }

            public object Clone()
            {
                var avion = new Avion
                {
                    
                    BrLeta = this.brLeta,
                    SBiznis = this.sBiznis,
                    SEkonomske = this.sEkonomske,
                    AKompanija = this.aKompanija,
                    Aktivan = this.aktivan


                };
                return avion;
            }

        }
    }

