﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Let : INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string pilot;

        public string Pilot
        {
            get { return pilot; }
            set { pilot = value; Changed("Pilot"); }
        }
        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; Changed("Sifra"); }
        }

        private string destinacija;

        public string Destinacija
        {
            get { return destinacija; }
            set { destinacija = value; Changed("Destinacija"); }
        }
        private string odrediste;

        public string Odrediste
        {
            get { return odrediste; }
            set { odrediste = value; Changed("Odrediste"); }
        }

        private DateTime vrijemePolaska;


        public DateTime VrijemePolaska
        {
            get { return vrijemePolaska; }
            set { vrijemePolaska = value; Changed("VrijemePolaska"); }
        }
        private DateTime vrijemeDolaska;

        public DateTime VrijemeDolaska
        {
            get { return vrijemeDolaska; }
            set { vrijemeDolaska = value; Changed("VrijemeDolaska"); }
        }

        private double cijena;

        public double Cijena
        {
            get { return cijena; }
            set { cijena = value; Changed("Cijena"); }
        }

        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; Changed("Aktivan"); }
        }






        private void Changed(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }
        public object Clone()
        {

            Let let = new Let
            {
                Pilot = this.pilot,
                Sifra = this.sifra,
                Destinacija = this.destinacija,
                Odrediste = this.odrediste,
                VrijemePolaska = this.vrijemePolaska,
                VrijemeDolaska = this.vrijemeDolaska,
                Cijena = this.cijena,
                Aktivan = this.aktivan


            };
            return let;


        }
    }
}
