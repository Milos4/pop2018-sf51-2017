﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.Sortiranje
{
    /// <summary>
    /// Interaction logic for SortiranjeKarata.xaml
    /// </summary>
    public partial class SortiranjeKarata : Window
    {
        public ObservableCollection<Karta> SortiranjeRastuci { get; set; }
        public ObservableCollection<Karta> SortiranjeOpadajuci { get; set; }
        public SortiranjeKarata()
        {
            InitializeComponent();
        }

        private void BtnRastuci_Click(object sender, RoutedEventArgs e)
        {
            DGKarta.ItemsSource = null;
            SortiranjeRastuci = new ObservableCollection<Karta>();
            {
                for (int i = 0; i < Data.Instance.Karte.Count; i++)
                {
                    for (int j = i; j < Data.Instance.Karte.Count; j++)
                    {
                        if (Data.Instance.Karte[i].Cijena.CompareTo(Data.Instance.Karte[j].Cijena) > 0)
                        {
                            var temp = Data.Instance.Karte[i];
                            Data.Instance.Karte[i] = Data.Instance.Karte[j];
                            Data.Instance.Karte[j] = temp;

                        }
                    }
                }
                SortiranjeRastuci = Data.Instance.Karte;
                DGKarta.ItemsSource = SortiranjeRastuci;
            }
        }


        private void BtnOpadajuci_Click(object sender, RoutedEventArgs e)
        {
            DGKarta.ItemsSource = null;
            SortiranjeOpadajuci = new ObservableCollection<Karta>();
            {
                for (int i = 0; i < Data.Instance.Karte.Count; i++)
                {
                    for (int j = i; j < Data.Instance.Karte.Count; j++)
                    {
                        if (Data.Instance.Karte[i].Cijena.CompareTo(Data.Instance.Karte[j].Cijena) < 0)
                        {
                            var temp = Data.Instance.Karte[i];
                            Data.Instance.Karte[i] = Data.Instance.Karte[j];
                            Data.Instance.Karte[j] = temp;

                        }
                    }
                }
                SortiranjeOpadajuci = Data.Instance.Karte;
                DGKarta.ItemsSource = SortiranjeOpadajuci;
            }
        }
    }
}
