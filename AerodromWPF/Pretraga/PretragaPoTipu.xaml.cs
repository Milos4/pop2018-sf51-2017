﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.Pretraga
{
    /// <summary>
    /// Interaction logic for PretragaPoTipu.xaml
    /// </summary>
    public partial class PretragaPoTipu : Window
    {
        public ObservableCollection<Korisnik> KorisniciPoTipu { get; set; }
        public PretragaPoTipu()
        {
            InitializeComponent();
            CBTip.ItemsSource = Enum.GetValues(typeof(Tip));
            CBTip.SelectedItem = Tip.Korisnik;
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGTip.ItemsSource = null;
            KorisniciPoTipu = new ObservableCollection<Korisnik>();
            Tip tip = (Tip) CBTip.SelectedItem;
            string tip1 = tip.ToString();
            foreach (Korisnik korisnik in Data.Instance.Korisnici)
            {
                if (korisnik.Tip == tip1)
                {
                    KorisniciPoTipu.Add(korisnik);

                }

            }
            DGTip.ItemsSource = KorisniciPoTipu;
        }
    }
}
