﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.KorisnikFolder
{
    /// <summary>
    /// Interaction logic for IspisLetova.xaml
    /// </summary>
    public partial class IspisLetova : Window
    {
        Korisnik korisnik;
        private ObservableCollection<Let> ListaLetova;


        public IspisLetova(Korisnik korisnik, ObservableCollection<Let> listaLetova)
        {
            InitializeComponent();
            this.korisnik = korisnik;
            this.ListaLetova = listaLetova;
            DGListaLetova.ItemsSource = ListaLetova;
        }

        private void BtnDalje_Click(object sender, RoutedEventArgs e)
        {
            Let let = (Let)DGListaLetova.SelectedItem;

            if (SelektovanLet(let))
            {
                Let stari = let.Clone() as Let;
                RezervacijaKarte edit = new RezervacijaKarte(let, korisnik);

                edit.Show();
                this.Close();
            //    if ((edit.ShowDialog()) == true)
            //    {
            //        int index = IndexLet(stari);
            //    }
            }
        }

        private void BtnNazad_Click(object sender, RoutedEventArgs e)
        {

        }

        private int IndexLet(Let let)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.Letovi.Count; i++)
            {
                if (Data.Instance.Letovi[i].Sifra.Equals(let.Sifra))
                {
                    index = i;

                }

            }
            return index;

        }
        private bool SelektovanLet(Let let)
        {
            if (let == null)
            {
                MessageBox.Show("Morate izabrati let! ");
                return false;
            }
            return true;
        }
    }
}
