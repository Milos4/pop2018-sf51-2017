﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaLetova
{
    /// <summary>
    /// Interaction logic for PretragaPoCeni.xaml
    /// </summary>
    public partial class PretragaPoCijeni : Window
    {
        public ObservableCollection<Let> pretragaPoCijeni { get; set; }
        public PretragaPoCijeni()
        {

            InitializeComponent();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGCijena.ItemsSource = null;
            pretragaPoCijeni = new ObservableCollection<Let>();
            double min = Double.Parse(TxtCijenaMin.Text);
            double max = Double.Parse(TxtCijenaMax.Text);

            foreach (Let let in Data.Instance.Letovi)
            {
                if (let.Cijena >= min && let.Cijena <= max)
                {
                    pretragaPoCijeni.Add(let);

                }

            }
            DGCijena.ItemsSource = pretragaPoCijeni;
        }
    }
}
