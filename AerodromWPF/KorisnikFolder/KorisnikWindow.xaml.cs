﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AerodromWPF.Database;
using AerodromWPF.Model;
using AerodromWPF.Registracija;

namespace AerodromWPF.KorisnikFolder
{
    /// <summary>
    /// Interaction logic for KorisnikWindow.xaml
    /// </summary>
    public partial class KorisnikWindow : Window
    {
        private Korisnik korisnik;

        public KorisnikWindow(Korisnik korisnik)
        {
            this.korisnik = korisnik;

            InitializeComponent();

            LblIme.Content = korisnik.KIme;
        }

        private void BtnPregled_Click(object sender, RoutedEventArgs e)
        {
            PregledKarata pk = new PregledKarata(korisnik);
            pk.ShowDialog();
        }

        private void BtnIzmjena_Click(object sender, RoutedEventArgs e)
        {
            Korisnik stari = korisnik.Clone() as Korisnik;
            PregledProfilaWindow edit = new PregledProfilaWindow(korisnik,stari);
            edit.ShowDialog();
        }

        private void BtnRezervacija_Click(object sender, RoutedEventArgs e)
        {
            RezervacijaKarteKorisnik edit = new RezervacijaKarteKorisnik(korisnik);
            edit.ShowDialog();
        }
    
    public int IndexKorisnika(Korisnik korisnik)
    {
        var index = -1;
        for (int i = 0; i < Data.Instance.Korisnici.Count; i++)
        {
            if (Data.Instance.Korisnici[i].KIme.Equals(korisnik.KIme))
            {
                index = i;

            }
        }
        return index;
    }

        private void BtnOdjava_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow lw = new LoginWindow();
            lw.Show();
            this.Close();
        }
    }
}
