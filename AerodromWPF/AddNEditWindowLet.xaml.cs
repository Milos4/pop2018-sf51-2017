﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for EditLet.xaml
    /// </summary>
    public partial class AddNEditWindowLet : Window
    {
        public enum EOpcija { DODAVANJE, IZMJENA };
        private EOpcija opcija;
        private Let let;
        public AddNEditWindowLet(Let let, EOpcija opcija)
        {
            this.let = let;
            this.opcija = opcija;
            this.DataContext = let;
            InitializeComponent();

            DestinacijaCB.ItemsSource = Data.Instance.Aerodromi;
            DestinacijaCB.DisplayMemberPath = "Grad";

            OdredisteCB.ItemsSource = Data.Instance.Aerodromi;
            OdredisteCB.DisplayMemberPath = "Grad";



            if (opcija.Equals(EOpcija.IZMJENA))
            {
                let.Odrediste = let.Odrediste.ToString();
                let.Destinacija = let.Destinacija.ToString();
                let.VrijemeDolaska = let.VrijemeDolaska;
                let.VrijemePolaska = let.VrijemePolaska;
                TxtSifra.IsEnabled = false;
            }
        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            Aerodrom odrediste = (Aerodrom)OdredisteCB.SelectedItem;
            Aerodrom destinacija = (Aerodrom)DestinacijaCB.SelectedItem;
            string polazak = PolazakDP.ToString();
            string dolazak = DolazakDP.ToString();
            let.Odrediste = odrediste.Grad.ToString();
            let.Destinacija = destinacija.Grad.ToString();
            let.VrijemeDolaska = DateTime.Parse(dolazak);
            let.VrijemePolaska = DateTime.Parse(polazak);
            let.Aktivan = true;

            if (opcija.Equals(EOpcija.IZMJENA))
            {
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                {
                    string insert = "update Let set Pilot='"
                         + let.Pilot + "',Sifra='" + let.Sifra + "',Destinacija='" + let.Destinacija + "',Odrediste='" + let.Odrediste + "',VrijemePolaska='" + let.VrijemePolaska + "',VrijemeDolaska='" + let.VrijemeDolaska
                        + "',Cijena='" + let.Cijena  + "',Aktivan='" + let.Aktivan + "' where Sifra='" + let.Sifra + "' ; ";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    con.Open();

                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

            if (opcija.Equals(EOpcija.DODAVANJE) && !Postoji(let.Sifra))
            {
                let.Odrediste = odrediste.Grad.ToString();
                let.Destinacija = destinacija.Grad.ToString();
                let.VrijemeDolaska = DateTime.Parse(dolazak);
                let.VrijemePolaska = DateTime.Parse(polazak);
                DateTime vrijemeDol = let.VrijemeDolaska;
                DateTime vrijemePol = let.VrijemePolaska;
                let.Aktivan = true;

                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                {
                    string insert = "insert into Let(Pilot,Sifra,Destinacija,Odrediste,VrijemePolaska,VrijemeDolaska,Cijena,Aktivan) values('"
                         + let.Pilot + "','" + let.Sifra + "','" + let.Destinacija + "','" + let.Odrediste + "','" + let.VrijemePolaska + "','" + let.VrijemeDolaska
                        + "','" + let.Cijena + "','" + let.Aktivan  + "') ";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    con.Open();

                    cmd.ExecuteNonQuery();
                    con.Close();
                    Data.Instance.Letovi.Add(let);

                }
            }
        }
        private void Odustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }
        public bool Postoji(string sifra)
        {
            foreach (Let let in Data.Instance.Letovi)
            {
                if (let.Sifra.Equals(sifra))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
