﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class AKompanija : INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string sifra;

        public string Sifra
        {
            get { return sifra; }
            set { sifra = value; Changed("Sifra"); }
        }

        private string naziv;

        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; Changed("Naziv"); }
        }


        private Boolean aktivan;

        public Boolean Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; Changed("Aktivan"); }
        }

        public void Changed(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }

        public object Clone()
        {
            var aKompanija = new AKompanija
            {
                Sifra = this.sifra,
                Naziv = this.naziv,
                Aktivan = this.aktivan

            };
            return aKompanija;
        }


    }
}
