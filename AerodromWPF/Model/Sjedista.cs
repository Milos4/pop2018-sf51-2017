﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public class Sjedista : INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private List<int> brZausetih;

        public List<int> BrZausetih
        {
            get { return brZausetih; }
            set { brZausetih = value; Changed("BrZausetih"); }
        }
        private List<int> brSlobodnih;

        public List<int> BrSlobodnih
        {
            get { return brSlobodnih; }
            set { brSlobodnih = value; Changed("BrSlobodnih"); }
        }

        public void Changed(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }

        public void PopuniBiznis(int b)
        {
            int x, y;
            x = b / 10;
            y = b - x * 10;
            for (int i = 1; i < x+1; i++)
            {
                for (int j = 1; j < y+1; j++)
                {
                    int c;
                    c = i * 10 + j;
                    BrSlobodnih.Add(c);
                }
            }
        }
        public void PopuniEkonomska(int ek)
        {
            int q, w;
            q = ek / 10;
            w = ek - q * 10;
            for (int k = 0; k < q+1; k++)
            {
                for (int j = 0; j < w+1; j++)
                {
                    int c;
                    c = k * 10 + j;
                    BrSlobodnih.Add(c);
                }
            }
        }



        public object Clone()
        {
            var sjedista = new Sjedista
            {

                BrSlobodnih = this.brSlobodnih,
                BrZausetih = this.brZausetih
            };
            return sjedista;
        }
     

    }
}