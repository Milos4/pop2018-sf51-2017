﻿using AerodromWPF.Sortiranje;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for SortiranjeWindow.xaml
    /// </summary>
    public partial class SortiranjeWindow : Window
    {
        public SortiranjeWindow()
        {
            InitializeComponent();
        }

        private void BtnIme_Click(object sender, RoutedEventArgs e)
        {
            SortiranjePoImenu soi = new SortiranjePoImenu();
            soi.Show();
            this.Close();
        }

        private void BtnPrezime_Click(object sender, RoutedEventArgs e)
        {
            SortirajPoPrezimenu sop = new SortirajPoPrezimenu();
            sop.Show();
            this.Close();
        }

        private void BtnLetovi_Click(object sender, RoutedEventArgs e)
        {
            SortiranjeLeta sol = new SortiranjeLeta();
            sol.Show();
            this.Close();
        }

        private void BtnKarte_Click(object sender, RoutedEventArgs e)
        {
            SortiranjeKarata sok = new SortiranjeKarata();
            sok.Show();
            this.Close();
        }
    }
}
