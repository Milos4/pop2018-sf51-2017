﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.KorisnikFolder
{
    /// <summary>
    /// Interaction logic for NeregistrovanKorisnik.xaml
    /// </summary>
    public partial class NeregistrovanKorisnik : Window
    {
        public NeregistrovanKorisnik()
        {
            InitializeComponent();
        }

        private void BtnLetovi_Click(object sender, RoutedEventArgs e)
        {
            RezervacijaKarteNeregistrovan rkk = new RezervacijaKarteNeregistrovan();
            rkk.ShowDialog();
        }
    }
}
