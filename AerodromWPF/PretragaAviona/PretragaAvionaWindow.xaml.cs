﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaAviona
{
    /// <summary>
    /// Interaction logic for PretragaAvionaWindow.xaml
    /// </summary>
    public partial class PretragaAvionaWindow : Window
    {
        public PretragaAvionaWindow()
        {
            InitializeComponent();
        }

        private void BtnBRleta_Click(object sender, RoutedEventArgs e)
        {
            PretragaPoBrojuLeta pob = new PretragaPoBrojuLeta();
            pob.Show();
            this.Close();
        }

        private void BtnAvioK_Click(object sender, RoutedEventArgs e)
        {
            PretragaPoAKompanijama poa = new PretragaPoAKompanijama();
            poa.Show();
            this.Close();
        }
    }
}
