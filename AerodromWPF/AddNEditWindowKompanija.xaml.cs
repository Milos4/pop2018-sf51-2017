﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for AddNEditWindowKompanija.xaml
    /// </summary>
    public partial class AddNEditWindowKompanija : Window
    {
        public ObservableCollection<Avion> AvioniJedneKompanije { get; set; }
        public ObservableCollection<Let> LetoviJedneKompanije { get; set; }
        public enum EOpcija { DODAVANJE, IZMJENA };
        private EOpcija opcija;
        private AKompanija aKompanija;
        public AddNEditWindowKompanija(AKompanija aKompanija, EOpcija opcija)
        {
            this.aKompanija = aKompanija;
            this.opcija = opcija;
            this.DataContext = aKompanija;
            InitializeComponent();





            if (opcija.Equals(EOpcija.IZMJENA))
            {
                AvioniJedneKompanije = new ObservableCollection<Avion>();
                LetoviJedneKompanije = new ObservableCollection<Let>();
                Sifra.IsEnabled = false;
                foreach(Avion avion in Data.Instance.Avioni)
                {
                    if(avion.AKompanija == aKompanija.Naziv)
                    {
                        AvioniJedneKompanije.Add(avion);
                        foreach (Let let in Data.Instance.Letovi)
                        {
                            if (let.Sifra == avion.BrLeta)
                            {
                                LetoviJedneKompanije.Add(let);

                            }
                        }
                }
                    DGLetoviKompanije.ItemsSource = LetoviJedneKompanije;
                }
                
            }
        }

        private void Sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            if (opcija.Equals(EOpcija.IZMJENA))
            {
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                {
                    string insert = "update AKompanija set Sifra='"
                         + aKompanija.Sifra + "',Naziv='" + aKompanija.Naziv + "',Aktivan='" + aKompanija.Aktivan + "' where Sifra='" + aKompanija.Sifra + "' ; ";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    con.Open();

                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }


            if (opcija.Equals(EOpcija.DODAVANJE) && !PostojiKompanija(aKompanija.Sifra))
            {
                aKompanija.Aktivan = true;
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                {
                    string insert = "insert into AKompanija(Sifra,Naziv,Aktivan) values('"
                         + aKompanija.Sifra + "','" + aKompanija.Naziv + "','" + aKompanija.Aktivan + "') ";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    con.Open();

                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                Data.Instance.AKompanije.Add(aKompanija);
            }


        }

        private void Odustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }
        private bool PostojiKompanija(string sifra)
        {

            foreach (AKompanija aKompanija in Data.Instance.AKompanije)
            {
                if (aKompanija.Sifra.Equals(Sifra))
                {
                    return true;

                }

            }
            return false;
        }
    }
}
