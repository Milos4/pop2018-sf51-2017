﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.Pretraga
{
    /// <summary>
    /// Interaction logic for PretragaKorisnika.xaml
    /// </summary>
    public partial class PretragaKorisnika : Window
    {
        public PretragaKorisnika()
        {
            InitializeComponent();
        }

        private void BtnIme_Click(object sender, RoutedEventArgs e)
        {
            PretragaPoImenu poi = new PretragaPoImenu();
            poi.Show();
            this.Close();

        }

        private void BtnPrezime_Click(object sender, RoutedEventArgs e)
        {
            PretragaPoPrezimenu pop = new PretragaPoPrezimenu();
            pop.Show();
            this.Close();
        }

        private void BtnKime_Click(object sender, RoutedEventArgs e)
        {
            PretragaPoKimenu pok = new PretragaPoKimenu();
            pok.Show();
            this.Close();
        }

        private void BtnTip_Click(object sender, RoutedEventArgs e)
        {
            PretragaPoTipu pot = new PretragaPoTipu();
            pot.Show();
            this.Close();
        }
    }
}
