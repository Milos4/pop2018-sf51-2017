﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.Pretraga
{
    /// <summary>
    /// Interaction logic for PretragaPoImenu.xaml
    /// </summary>
    public partial class PretragaPoImenu : Window
    {
        public ObservableCollection<Korisnik> KorisniciPoImenu { get; set; }
        public PretragaPoImenu()
        {

            InitializeComponent();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGIme.ItemsSource = null;
            KorisniciPoImenu = new ObservableCollection<Korisnik>();
            string ime = TxtIme.Text;
            foreach (Korisnik korisnik in Data.Instance.Korisnici)
            {
                if (korisnik.Ime == ime)
                {
                    KorisniciPoImenu.Add(korisnik);

                }

            }
            DGIme.ItemsSource = KorisniciPoImenu;
        }
    }
}
