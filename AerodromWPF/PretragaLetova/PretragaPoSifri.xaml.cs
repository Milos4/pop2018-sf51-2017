﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaLetova
{
    /// <summary>
    /// Interaction logic for PretragaPoSifri.xaml
    /// </summary>
    public partial class PretragaPoSifri : Window
    {
        public ObservableCollection<Let> LetPoSifri { get; set; }
        public PretragaPoSifri()
        {

            InitializeComponent();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGSifra.ItemsSource = null;
            LetPoSifri = new ObservableCollection<Let>();
            string sifra = TxtSifra.Text;
            foreach (Let let in Data.Instance.Letovi)
            {
                if (let.Sifra == sifra)
                {
                    LetPoSifri.Add(let);

                }

            }
            DGSifra.ItemsSource = LetPoSifri;
        }
    }
}
