﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{
    public enum Klasa { Biznis, Ekonomska }
    public class Karta : INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string brLeta;

        public string BrLeta
        {
            get { return brLeta; }
            set { brLeta = value; Changed("BrLeta"); }
        }
        private string brSjedista;

        public string BrSjedista
        {
            get { return brSjedista; }
            set { brSjedista = value; Changed("BrSjedista"); }
        }

        private string  nKorisnika;

        public string NKorisnika
        {
            get { return nKorisnika; }
            set { nKorisnika = value; Changed("NKorisnika"); }
        }

        private string klasa;

        public string Klasa
        {
            get { return klasa; }
            set { klasa = value; Changed("Klasa"); }
        }

        private string kapija;

        public string Kapija
        {
            get { return kapija; }
            set { kapija = value; Changed("Kapija"); }
        }

        private double cijena;



        public double Cijena
        {
            get { return cijena; }
            set { cijena = value; Changed("Cijena"); }
        }
        private Boolean aktivan;

        public Boolean Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; Changed("Aktivan"); }
        }

        public void Changed(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }

        public object Clone()
        {
            var karta = new Karta
            {
                BrLeta = this.brLeta,
                BrSjedista = this.brSjedista,
                NKorisnika = this.nKorisnika,
                Klasa = this.klasa,
                Kapija = this.kapija,
                Cijena = this.cijena,
                Aktivan = this.aktivan

            };
            return karta;
        }

        public override string ToString()
        {
            return $"Br. Leta: {BrLeta}, Sjediste: {BrSjedista},Korisnik: {NKorisnika},Klasa: {Klasa},Kapija: {Kapija},Cijena: {Cijena}";
        }
    }
}
