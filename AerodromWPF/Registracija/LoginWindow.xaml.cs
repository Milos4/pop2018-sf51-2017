﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AerodromWPF.KorisnikFolder;

namespace AerodromWPF.Registracija
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void Korisnik_Click(object sender, RoutedEventArgs e)
        {
            LoginKorisnikAdministrator edit = new LoginKorisnikAdministrator(LoginKorisnikAdministrator.EOpcija.KORISNIK);
            edit.Show();
            this.Close();

        }

        private void Administrator_Click(object sender, RoutedEventArgs e)
        {
            LoginKorisnikAdministrator edit = new LoginKorisnikAdministrator(LoginKorisnikAdministrator.EOpcija.ADMINISTRATOR);
            edit.Show();
            this.Close();
        }

        private void Neregistrovan_Click(object sender, RoutedEventArgs e)
        {
            NeregistrovanKorisnik edit = new NeregistrovanKorisnik();
            edit.ShowDialog();
            
        }
    }
}
