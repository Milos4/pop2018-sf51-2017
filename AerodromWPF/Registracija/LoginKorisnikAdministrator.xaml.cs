﻿using AerodromWPF.Database;
using AerodromWPF.KorisnikFolder;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.Registracija
{
    /// <summary>
    /// Interaction logic for LoginKorisnikAdministrator.xaml
    /// </summary>
    public partial class LoginKorisnikAdministrator : Window
    {
        Korisnik PronadjeniKorisnik;
        public enum EOpcija { KORISNIK, ADMINISTRATOR };
        private EOpcija opcija;
        public LoginKorisnikAdministrator(EOpcija opcija)
        {
            
            this.opcija = opcija;
            InitializeComponent();
            
        }


        private void OK_Click(object sender, RoutedEventArgs e)
        {
            string korisnickoIme = KIme.Text;
            string lozinka = Lozinka.Text;
            if (opcija.Equals(EOpcija.KORISNIK))
            {
                if (PostojiKorisnik(korisnickoIme) && PronadjeniKorisnik.Aktivan == true)
                {
                    if (PostojiLozinka(lozinka) && PronadjeniKorisnik.Tip.Equals("Korisnik"))
                    {
                        KorisnikWindow korisnikWindow = new KorisnikWindow(PronadjeniKorisnik);
                        korisnikWindow.Show();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Pogresna lozinka ili tip! ");
                    }
                }
                else
                {
                    MessageBox.Show("Korisnik nepostoji! ");
                }
            }
            else
            {
                if (PostojiKorisnik(korisnickoIme) && PronadjeniKorisnik.Aktivan == true)
                {
                    if (PostojiLozinka(lozinka) && PronadjeniKorisnik.Tip.Equals("Admin"))
                    {
                        MainWindow edit = new MainWindow();
                        edit.Show();
                        this.Close();

                    }
                    else
                    {
                        MessageBox.Show("Pogresna lozinka ili tip! ");
                    }
                }
                else
                {
                    MessageBox.Show("Korisnik nepostoji! ");
                }
            }
        }

        private void Nazad_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow lw = new LoginWindow();
            lw.Show();
            this.Close();
        }

        private bool PostojiKorisnik(string sifra)
        {

            foreach (Korisnik korsnik in Data.Instance.Korisnici)
            {
                if (korsnik.KIme.Equals(sifra)) 
                {
                    PronadjeniKorisnik = korsnik;
                    return true;

                }

            }
            return false;
        }

        private bool PostojiLozinka(string sifra1)
        {
                if (PronadjeniKorisnik.Lozinka.Equals(sifra1))
                {
                    return true;

                }

            
            return false;
        }
    }
}