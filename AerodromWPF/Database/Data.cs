﻿using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Data;

namespace AerodromWPF.Database
{
    class Data
    {

        public ObservableCollection<Karta> Karte { get; set; }
        public ObservableCollection<Korisnik> Korisnici { get; set; }
        public ObservableCollection<Aerodrom> Aerodromi { get; set; }
        public ObservableCollection<Let> Letovi { get; set; }
        public ObservableCollection<Avion> Avioni { get; set; }
        public String UlogovanKorisnik { get; set; }
        public ObservableCollection<AKompanija> AKompanije { get; set; }

        private Data()
        {
            Karte = new ObservableCollection<Karta>();
            Korisnici = new ObservableCollection<Korisnik>();
            Aerodromi = new ObservableCollection<Aerodrom>();
            Letovi = new ObservableCollection<Let>();
            Avioni = new ObservableCollection<Avion>();
            AKompanije = new ObservableCollection<AKompanija>();
            UcitajSveAerodrome();
            UcitajSveKorisnike();
            UcitajLetove();
            UcitajAvione();
            UcitajKompanije();
            UcitajKarte();
        }

        private static Data _instance = null;

        public static Data Instance
        {

            get
            {
                if (_instance == null)
                    _instance = new Data();
                return _instance;
            }
        }

        public void UcitajSveKorisnike()
        {
            using (SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True"))
                
            {
                con.Open();
                DataSet ds = new DataSet();

                SqlCommand korisnikKom = con.CreateCommand();
                korisnikKom.CommandText = @"Select distinct * from Korisnik";
                SqlDataAdapter daKorisnik = new SqlDataAdapter();
                daKorisnik.SelectCommand = korisnikKom;
                daKorisnik.Fill(ds, "Korisnik");

                foreach (DataRow row in ds.Tables["Korisnik"].Rows)
                {
                    Korisnik k = new Korisnik();
                    k.KIme = (string)row["KIme"];
                    k.Lozinka = (string)row["Lozinka"];
                    k.Ime = (string)row["Ime"];
                    k.Prezime = (string)row["Prezime"];
                    k.Email = (string)row["Email"];
                    k.Adresa = (string)row["Adresa"];
                    k.Pol = (string)row["Pol"];
                    k.Tip = (string)row["Tip"];
                    k.Aktivan = (bool)row["Aktivan"];

                    Korisnici.Add(k);
                    

                }
                con.Close();
            }

            
        }

        public void UcitajSveAerodrome()
        {
            using (SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True"))

            {
                con.Open();
                DataSet ds = new DataSet();

                SqlCommand aerodromKom = con.CreateCommand();
                aerodromKom.CommandText = @"Select distinct * from Aerodrom";
                SqlDataAdapter daAerodrom = new SqlDataAdapter();
                daAerodrom.SelectCommand = aerodromKom;
                daAerodrom.Fill(ds, "Aerodrom");

                foreach (DataRow row in ds.Tables["Aerodrom"].Rows)
                {
                    Aerodrom a = new Aerodrom();
                    a.Sifra = (string)row["Sifra"];
                    a.Naziv = (string)row["Naziv"];
                    a.Grad = (string)row["Grad"];
                    a.Aktivan = (Boolean)row["Aktivan"];

                    Aerodromi.Add(a);


                }
                con.Close();
            }


        }

        private void UcitajLetove()
        {
            using (SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True"))

            {
                con.Open();
                DataSet ds = new DataSet();

                SqlCommand letoviKom = con.CreateCommand();
                letoviKom.CommandText = @"Select distinct * from Let";
                SqlDataAdapter daLet = new SqlDataAdapter();
                daLet.SelectCommand = letoviKom;
                daLet.Fill(ds, "Let");

                foreach (DataRow row in ds.Tables["Let"].Rows)
                {
                    Let l = new Let();
                    l.Pilot = (string)row["Pilot"];
                    l.Sifra = (string)row["Sifra"];
                    l.Destinacija = (string)row["Destinacija"];
                    l.Odrediste = (string)row["Odrediste"];
                    l.VrijemePolaska = DateTime.Parse(row["VrijemePolaska"].ToString());
                    l.VrijemeDolaska = DateTime.Parse(row["VrijemeDolaska"].ToString());
                    l.Cijena = (double)row["Cijena"];
                    l.Aktivan = (Boolean)row["Aktivan"];

                    Letovi.Add(l);


                }
                con.Close();
            }
        }

        private void UcitajAvione()
        {
            using (SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True"))

            {
                con.Open();
                DataSet ds = new DataSet();

                SqlCommand avionkom = con.CreateCommand();
                avionkom.CommandText = @"Select distinct * from Avion";
                SqlDataAdapter daAvion = new SqlDataAdapter();
                daAvion.SelectCommand = avionkom;
                daAvion.Fill(ds, "Avion");

                foreach (DataRow row in ds.Tables["Avion"].Rows)
                {
                    Avion a = new Avion();
                    a.BrLeta = (string)row["BrLeta"];
                    a.SBiznis = (int)row["SBiznis"];
                    a.SEkonomske = (int)row["SEkonomske"];
                    a.AKompanija = (string)row["AKompanija"];
                    a.Aktivan = (Boolean)row["Aktivan"];

                    Avioni.Add(a);


                }
                con.Close();
            }
        }
        private void UcitajKompanije()
        {
            using (SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True"))

            {
                con.Open();
                DataSet ds = new DataSet();

                SqlCommand kompanijekom = con.CreateCommand();
                kompanijekom.CommandText = @"Select distinct * from AKompanija";
                SqlDataAdapter daKompanija = new SqlDataAdapter();
                daKompanija.SelectCommand = kompanijekom;
                daKompanija.Fill(ds, "AKompanija");

                foreach (DataRow row in ds.Tables["AKompanija"].Rows)
                {
                    AKompanija a = new AKompanija();
                    a.Sifra = (string)row["Sifra"];
                    a.Naziv = (string)row["Naziv"];
                    a.Aktivan = (Boolean)row["Aktivan"];

                    AKompanije.Add(a);


                }
                con.Close();
            }

        }

        private void UcitajKarte()
        {
            using (SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True"))

            {
                con.Open();
                DataSet ds = new DataSet();

                SqlCommand kartakom = con.CreateCommand();
                kartakom.CommandText = @"Select distinct * from Karta";
                SqlDataAdapter daKarta = new SqlDataAdapter();
                daKarta.SelectCommand = kartakom;
                daKarta.Fill(ds, "Karta");

                foreach (DataRow row in ds.Tables["Karta"].Rows)
                {
                    Karta k = new Karta();
                    k.BrLeta = (string)row["BrLeta"];
                    k.BrSjedista = (string)row["BrSjedista"];
                    k.NKorisnika = (string)row["NKorisnika"];
                    k.Klasa = (string)row["Klasa"];
                    k.Kapija = (string)row["Kapija"];
                    k.Cijena = (double)row["Cijena"];
                    k.Aktivan = (Boolean)row["Aktivan"];

                    Karte.Add(k);


                }
                con.Close();
            }
        }
        //    private void UcitajKarte()
        //{
        //    Karte.Add(new Karta
        //    {
        //        BrLeta = "1234",
        //        BrSjedista = "16",
        //        NKorisnika = "Mike123",
        //        Klasa = "Biznis",
        //        Kapija = "1A",
        //        Cijena = 6000,
        //        Aktivan = true


        //    });
        //    Karte.Add(new Karta
        //    {
        //        BrLeta = "1234",
        //        BrSjedista = "15",
        //        NKorisnika = "Mike123",
        //        Klasa = "Biznis",
        //        Kapija = "1B",
        //        Cijena = 6500,
        //        Aktivan = true


        //    });
        //}

        //public void SacuvajSveAerodrome()
        //{
        //    XmlWriter writer = XmlWriter.Create("..//..//Data//Aerodromi.xml");

        //    writer.WriteStartElement("aerodromi");
        //    foreach (Aerodrom aerodrom in Aerodromi)
        //    {
        //        writer.WriteStartElement("aerodrom");
        //        writer.WriteAttributeString("sifra", aerodrom.Sifra);
        //        writer.WriteAttributeString("naziv", aerodrom.Naziv);
        //        writer.WriteAttributeString("grad", aerodrom.Grad);
        //        writer.WriteEndElement();
        //    }

        //    writer.WriteEndDocument();
        //    writer.Close();


        //}
    
    }
}
