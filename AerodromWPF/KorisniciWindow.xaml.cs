﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
   
    /// <summary>
    /// Interaction logic for KorisniciWindow.xaml
    /// </summary>
    public partial class KorisniciWindow : Window
    {
        List<Korisnik> aktivniKorisnici;
        public KorisniciWindow()
        {
            aktivniKorisnici = new List<Korisnik>();
            InitializeComponent();
            foreach (Korisnik korisnik in Data.Instance.Korisnici)
            {
                
                if (korisnik.Aktivan == true)
                {
                    aktivniKorisnici.Add(korisnik);
                }

            }
            DGKorisnici.ItemsSource = aktivniKorisnici;
        }


        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AddNEditWindowKorisnik aewk = new AddNEditWindowKorisnik(new Korisnik() , AddNEditWindowKorisnik.EOpcija.DODAVANJE);
            aewk.ShowDialog();
        }

        private void BtnIzmjeni_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = (Korisnik)DGKorisnici.SelectedItem;


            if (SelektovanKorisnik(korisnik))
            {
                Korisnik stari = korisnik.Clone() as Korisnik;
                AddNEditWindowKorisnik edit = new AddNEditWindowKorisnik(korisnik, AddNEditWindowKorisnik.EOpcija.IZMJENA);
                if ((edit.ShowDialog()) == true)
                {
                    int index = IndexKorisnika(stari);
                }
            }
        }

        private void BtnIzbrisi_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = (Korisnik)DGKorisnici.SelectedItem;
            if (SelektovanKorisnik(korisnik) && korisnik.Aktivan) 
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                    {
                        korisnik.Aktivan = false;
                        string insert = "update Korisnik set Aktivan='"
                             + korisnik.Aktivan +  "' where KIme='" + korisnik.KIme + "' ; ";
                        SqlCommand cmd = new SqlCommand(insert, con);

                        con.Open();

                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }

        }



        public int IndexKorisnika(Korisnik korisnik)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.Korisnici.Count; i++)
            {
                if (Data.Instance.Korisnici[i].KIme.Equals(korisnik.KIme))
                {
                    index = i;

                }
            }
            return index;
        }

        private bool SelektovanKorisnik(Korisnik korisnik)
        {
            if (korisnik == null)
            {
                MessageBox.Show("Nije selektovan korisnik! ");
                return false;
            }
            return true;
        }
    }
}
