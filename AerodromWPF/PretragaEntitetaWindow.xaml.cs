﻿using AerodromWPF.Pretraga;
using AerodromWPF.PretragaAviona;
using AerodromWPF.PretragaKarata;
using AerodromWPF.PretragaLetova;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for PretragaEntitetaWindow.xaml
    /// </summary>
    public partial class PretragaEntitetaWindow : Window
    {
        public PretragaEntitetaWindow()
        {
            InitializeComponent();
        }

        private void BtnKorinika_Click(object sender, RoutedEventArgs e)
        {
            PretragaKorisnika pk = new PretragaKorisnika();
            pk.Show();
            this.Close();
        }

        private void BtnLetova_Click(object sender, RoutedEventArgs e)
        {
            PretragaLetova.PretragaLetova pl = new PretragaLetova.PretragaLetova();
            pl.Show();
            this.Close();
        }

        private void BtnAviona_Click(object sender, RoutedEventArgs e)
        {
            PretragaAvionaWindow paw = new PretragaAvionaWindow();
            paw.Show();
            this.Close();
        }

        private void BtnKarata_Click(object sender, RoutedEventArgs e)
        {
            PretragaKarataWindow paw = new PretragaKarataWindow();
            paw.Show();
            this.Close();
        }
    }
}
