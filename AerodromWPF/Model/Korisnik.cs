﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerodromWPF.Model
{ 
public enum Pol { Muski, Zenski }
public enum Tip { Admin, Korisnik }
public class Korisnik : INotifyPropertyChanged, ICloneable
{
    public event PropertyChangedEventHandler PropertyChanged;

    private string ime;

    public string Ime
    {
        get { return ime; }
        set { ime = value; Changed("Ime"); }
    }
    private string prezime;

    public string Prezime
    {
        get { return prezime; }
        set { prezime = value; Changed("Prezime"); }
    }
    private string email;

    public string Email
    {
        get { return email; }
        set { email = value; Changed("Email"); }
    }
    private string adresa;

    public string Adresa
    {
        get { return adresa; }
        set { adresa = value; Changed("Adresa"); }
    }
    private string kIme;

    public string KIme
        {
        get { return kIme; }
        set { kIme = value; Changed("KorisnickoIme"); }
    }
    private string lozinka;

    public string Lozinka
    {
        get { return lozinka; ; }
        set { lozinka = value; Changed("Loinka"); }
    }
    private string pol;

    public string Pol
    {
        get { return pol; }
        set { pol = value; Changed("Pol"); }
    }
    private string tip;

    public string Tip
    {
        get { return tip; }
        set { tip = value; Changed("TipKorisnika"); }
    }
    private Boolean aktivan;

    public Boolean Aktivan
    {
        get { return aktivan; }
        set { aktivan = value; Changed("Aktivan"); }
    }

    public void Changed(string name)
    {
        if (PropertyChanged != null)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

    }

    public object Clone()
    {
        var korisnik = new Korisnik
        {
            Ime = this.ime,
            Prezime = this.prezime,
            Email = this.email,
            Adresa = this.adresa,
            Lozinka = this.lozinka,
            Pol = this.pol,
            Tip = this.tip,
            KIme = this.kIme,
            Aktivan = this.aktivan

        };
        return korisnik;
    }
/*public override string ToString()
        {
            return $"Korisnicko ime: {KIme}, Tip korisnika: {Tip}";
        }*/
    }
}
