﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.Sortiranje
{
    /// <summary>
    /// Interaction logic for SortiranjeLeta.xaml
    /// </summary>
    public partial class SortiranjeLeta : Window
    {
        public ObservableCollection<Let> SortiranjeRastuci { get; set; }
        public ObservableCollection<Let> SortiranjeOpadajuci { get; set; }
        public SortiranjeLeta()
        {
            InitializeComponent();
        }

        private void BtnRastuci_Click(object sender, RoutedEventArgs e)
        {
            DGLet.ItemsSource = null;
            SortiranjeRastuci = new ObservableCollection<Let>();
            {
                for (int i = 0; i < Data.Instance.Letovi.Count; i++)
                {
                    for (int j = i; j < Data.Instance.Letovi.Count; j++)
                    {
                        if (Data.Instance.Letovi[i].Cijena.CompareTo(Data.Instance.Letovi[j].Cijena) > 0)
                        {
                            var temp = Data.Instance.Letovi[i];
                            Data.Instance.Letovi[i] = Data.Instance.Letovi[j];
                            Data.Instance.Letovi[j] = temp;

                        }
                    }
                }
                SortiranjeRastuci = Data.Instance.Letovi;
                DGLet.ItemsSource = SortiranjeRastuci;
            }
        }


        private void BtnOpadajuci_Click(object sender, RoutedEventArgs e)
        {
            DGLet.ItemsSource = null;
            SortiranjeOpadajuci = new ObservableCollection<Let>();
            {
                for (int i = 0; i < Data.Instance.Letovi.Count; i++)
                {
                    for (int j = i; j < Data.Instance.Letovi.Count; j++)
                    {
                        if (Data.Instance.Letovi[i].Cijena.CompareTo(Data.Instance.Letovi[j].Cijena) < 0)
                        {
                            var temp = Data.Instance.Letovi[i];
                            Data.Instance.Letovi[i] = Data.Instance.Letovi[j];
                            Data.Instance.Letovi[j] = temp;

                        }
                    }
                }
                SortiranjeOpadajuci = Data.Instance.Letovi;
                DGLet.ItemsSource = SortiranjeOpadajuci;
            }
        }
    }
}
