﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for Letovi.xaml
    /// </summary>
    public partial class LetWindow : Window
    {
        List<Let> aktivniLetovi;
        public LetWindow()
        {
            aktivniLetovi = new List<Let>();
            InitializeComponent();
            foreach (Let let in Data.Instance.Letovi)
            {

                if (let.Aktivan == true)
                {
                    aktivniLetovi.Add(let);
                }

            }
            DGLetovi.ItemsSource = aktivniLetovi;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AddNEditWindowLet edit = new AddNEditWindowLet(new Let(), AddNEditWindowLet.EOpcija.DODAVANJE);
            edit.ShowDialog();
        }

        private void BtnIzmjeni_Click(object sender, RoutedEventArgs e)
        {
            Let let = (Let)DGLetovi.SelectedItem;

            if (SelektovanLet(let))
            {
                Let stari = let.Clone() as Let;
                AddNEditWindowLet edit = new AddNEditWindowLet(let, AddNEditWindowLet.EOpcija.IZMJENA);
                if ((edit.ShowDialog()) == true)
                {
                    int index = IndexLet(stari);
                }
            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            Let let = (Let)DGLetovi.SelectedItem;

            if (SelektovanLet(let))
            {
                if (MessageBox.Show("Da li ste sigurni", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    let.Aktivan = false;
                    SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                    {
                        string insert = "update Let set Aktivan='"
                             + let.Aktivan +  "' where Sifra='" + let.Sifra + "' ; ";
                        SqlCommand cmd = new SqlCommand(insert, con);

                        con.Open();

                        cmd.ExecuteNonQuery();
                        con.Close();
                    }

                }
            }

        }
        private int IndexLet(Let let)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.Letovi.Count; i++)
            {
                if (Data.Instance.Letovi[i].Sifra.Equals(let.Sifra))
                {
                    index = i;

                }

            }
            return index;

        }
        private bool SelektovanLet(Let let)
        {
            if (let == null)
            {
                MessageBox.Show("Morate izabrati let! ");
                return false;
            }
            return true;
        }

      
    }
}
