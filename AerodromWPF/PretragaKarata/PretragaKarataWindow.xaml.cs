﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaKarata
{
    /// <summary>
    /// Interaction logic for PretragaKarataWindow.xaml
    /// </summary>
    public partial class PretragaKarataWindow : Window
    {
        public PretragaKarataWindow()
        {
            InitializeComponent();
        }

        private void BtnBrLeta_Click(object sender, RoutedEventArgs e)
        {
            PretragaPoLetu pol = new PretragaPoLetu();
            pol.Show();
            this.Close();
        }

        private void BtnNazivuK_Click(object sender, RoutedEventArgs e)
        {
            PretragaPoKorisniku pok = new PretragaPoKorisniku();
            pok.Show();
            this.Close();
        }

        private void BtnDestinacija_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnOdrediste_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnKlasi_Click(object sender, RoutedEventArgs e)
        {
            PretragaPoKlasi pok = new PretragaPoKlasi();
            pok.Show();
            this.Close();
        }
    }
}
