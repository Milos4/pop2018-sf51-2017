﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.PretragaLetova
{
    /// <summary>
    /// Interaction logic for PretragaLetova.xaml
    /// </summary>
    public partial class PretragaLetova : Window
    {
        public PretragaLetova()
        {
            InitializeComponent();
        }

        private void BtnSifri_Click(object sender, RoutedEventArgs e)
        {
            PretragaPoSifri pos = new PretragaPoSifri();
            pos.Show();
            this.Close();
        }

        private void BtnAvioK_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnDestinacija_Click(object sender, RoutedEventArgs e)
        {
            PretragaPoDestinaciji pod = new PretragaPoDestinaciji();
            pod.Show();
            this.Close();

        }

        private void BtnOdrediste_Click(object sender, RoutedEventArgs e)
        {
            PretragaPoOdredistu poo = new PretragaPoOdredistu();
            poo.Show();
            this.Close();

        }

        private void BtnCijena_Click(object sender, RoutedEventArgs e)
        {
            PretragaPoCijeni poc = new PretragaPoCijeni();
            poc.Show();
            this.Close();
        }
    }
}
