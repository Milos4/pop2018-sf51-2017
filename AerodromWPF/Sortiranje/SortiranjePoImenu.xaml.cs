﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.Sortiranje
{
    /// <summary>
    /// Interaction logic for SortiranjePoImenu.xaml
    /// </summary>
    public partial class SortiranjePoImenu : Window
    {
        public ObservableCollection<Korisnik> SortiranjeRastuci { get; set; }
        public ObservableCollection<Korisnik> SortiranjeOpadajuci { get; set; }
        public SortiranjePoImenu()
        {
            InitializeComponent();
        }

        private void BtnRastuci_Click(object sender, RoutedEventArgs e)
        {
            DGIme.ItemsSource = null;
            SortiranjeRastuci = new ObservableCollection<Korisnik>();
            {
                for (int i = 0; i < Data.Instance.Korisnici.Count; i++)
                {
                    for (int j = i; j < Data.Instance.Korisnici.Count; j++)
                    {
                        if (Data.Instance.Korisnici[i].Ime.CompareTo(Data.Instance.Korisnici[j].Ime) > 0)
                        {
                            var temp = Data.Instance.Korisnici[i];
                            Data.Instance.Korisnici[i] = Data.Instance.Korisnici[j];
                            Data.Instance.Korisnici[j] = temp;

                        }
                    }
                }
                SortiranjeRastuci = Data.Instance.Korisnici;
                DGIme.ItemsSource = SortiranjeRastuci;
            }
        }


        private void BtnOpadajuci_Click(object sender, RoutedEventArgs e)
        {
            DGIme.ItemsSource = null;
            SortiranjeOpadajuci = new ObservableCollection<Korisnik>();
            {
                for (int i = 0; i < Data.Instance.Korisnici.Count; i++)
                {
                    for (int j = i; j < Data.Instance.Korisnici.Count; j++)
                    {
                        if (Data.Instance.Korisnici[i].Ime.CompareTo(Data.Instance.Korisnici[j].Ime) < 0)
                        {
                            var temp = Data.Instance.Korisnici[i];
                            Data.Instance.Korisnici[i] = Data.Instance.Korisnici[j];
                            Data.Instance.Korisnici[j] = temp;

                        }
                    }
                }
                SortiranjeOpadajuci = Data.Instance.Korisnici;
                DGIme.ItemsSource = SortiranjeOpadajuci;
            }
        }
    }
}
