﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF
{
    /// <summary>
    /// Interaction logic for KartaWindow.xaml
    /// </summary>
    public partial class KartaWindow : Window
    {

        List<Karta> aktivneKarte;
        public KartaWindow()
        {
            aktivneKarte = new List<Karta>();
            InitializeComponent();
            foreach (Karta karta in Data.Instance.Karte)
            {

                if (karta.Aktivan == true)
                {
                    aktivneKarte.Add(karta);
                }

            }
            DGKarte.ItemsSource = aktivneKarte;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AddNEditWindowKarte edit = new AddNEditWindowKarte(new Karta(), AddNEditWindowKarte.EOpcija.DODAVANJE);
            edit.ShowDialog();

        }

        private void BtnIzmjeni_Click(object sender, RoutedEventArgs e)
        {
            Karta karta = (Karta)DGKarte.SelectedItem;

            if (SelektovanaKarta(karta))
            {
                Karta stari = karta.Clone() as Karta;
                AddNEditWindowKarte edit = new AddNEditWindowKarte(karta, AddNEditWindowKarte.EOpcija.IZMJENA);
                if ((edit.ShowDialog()) == true)
                {
                    int index = IndexKarte(stari);
                }
            }

        }

        private void BtnIzbrisi_Click(object sender, RoutedEventArgs e)
        {
            Karta karta = (Karta)DGKarte.SelectedItem;

            if (SelektovanaKarta(karta))
            {
                if (MessageBox.Show("Da li ste sigurni", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    karta.Aktivan = false;
                    SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\admin\Desktop\pop2018-sf51-2017-master\AerodromWPF\Baza\AerodromApp.mdf;Integrated Security=True");
                    {
                        string insert = "update Karta set Aktivan='"
                             + karta.Aktivan + "' where BrLeta ='" + karta.BrLeta + "' and BrSjedista ='" + karta.BrSjedista + "' ; ";
                        SqlCommand cmd = new SqlCommand(insert, con);

                        con.Open();

                        cmd.ExecuteNonQuery();
                        con.Close();
                    }

                }
            }

        }
        private int IndexKarte(Karta karta)
        {
            var index = -1;
            for (int i = 0; i < Data.Instance.Karte.Count; i++)
            {
                if (Data.Instance.Karte[i].NKorisnika.Equals(karta.NKorisnika))
                {
                    index = i;

                }

            }
            return index;

        }
        private bool SelektovanaKarta(Karta karta)
        {
            if (karta == null)
            {
                MessageBox.Show("Morate izabrati kartu");
                return false;
            }
            return true;
        }
    }
}
