﻿using AerodromWPF.Database;
using AerodromWPF.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerodromWPF.Pretraga
{
    /// <summary>
    /// Interaction logic for PretragaPoKimenu.xaml
    /// </summary>
    public partial class PretragaPoKimenu : Window
    {
        public ObservableCollection<Korisnik> KorisniciPoKimenu { get; set; }
        public PretragaPoKimenu()
        {

            InitializeComponent();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            DGKime.ItemsSource = null;
            KorisniciPoKimenu = new ObservableCollection<Korisnik>();
            string Kime = TxtKime.Text;
            foreach (Korisnik korisnik in Data.Instance.Korisnici)
            {
                if (korisnik.KIme == Kime)
                {
                    KorisniciPoKimenu.Add(korisnik);

                }

            }
            DGKime.ItemsSource = KorisniciPoKimenu;
        }
    }
}
